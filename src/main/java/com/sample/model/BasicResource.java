
package com.sample.model;

import java.io.Serializable;

public class BasicResource implements Serializable {
	
	protected transient String wid;

	public String getWid() {
		return wid;
	}

	public void setWid(String wid) {
		this.wid = wid;
	}
	
}
