package com.sample.model.constants;


public enum RangeCondition implements IEnum{

    EQ("EQ"),  //equal
    EQUAL("EQ"), //equal
    LT("LT"), //Less than
    GT("GT"), //Greater than
    BW("BETWEEN"), //between
    NE("NE"), //Not Equal
    IN("IN"), //In
    NC("NOT_CONTAINS"), //Not in
    GE("GE"), //Greater or equal
    LE("LE"), // Less or equal
    LIKE("EQ"),//Like
    NLIKE("NE"),//Not like
    LIKEWC("CONTAINS"), //Like with wildcards
    NLIKEWC("NOT_CONTAINS");//Not like with wildcards


    private final String comparisonOperator;

    RangeCondition(String op) {
        this.comparisonOperator = op;
    }

    public String getValue() {
        return comparisonOperator;
    }

    public static RangeCondition getEnum(String value) {
        if (value == null) {
            throw new IllegalArgumentException("value cannot be null");
        }

        if ("equal".equals(value)) {
            return EQ;
        } else if("less".equals(value)) {
            return  LT;
        } else if("greater".equals(value)) {
            return  GT;
        } else if("gte".equals(value)) {
            return  GE;
        } else if("between".equals(value)) {
            return  BW;
        }
        try {
            return RangeCondition.valueOf(value);
        } catch (Exception e) {
            throw new IllegalArgumentException("No corresponding ComparisonOperator found for the value [ "+value+" ]. Cause: " + e.getMessage());
        }
    }

}