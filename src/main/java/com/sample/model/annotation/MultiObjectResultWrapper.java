package com.sample.model.annotation;

import java.io.Serializable;

public class MultiObjectResultWrapper implements Serializable {

    private static final long serialVersionUID = -7756508234667089814L;

    Object[] rowObjects;

    public MultiObjectResultWrapper(Object[] rowObjects) {
        this.rowObjects = rowObjects;
    }

    public Object[] getRowObjects() {
        return rowObjects;
    }

}
