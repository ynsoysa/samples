package com.sample.model.annotation;

import java.util.LinkedHashMap;
import java.util.Map;

public class MultiClassColumnSetMapper {

    Map<Class, Map<String, String>> classColumnMapping = new LinkedHashMap<Class, Map<String,String>>();

    public void addClassColumnMapper(Class clazz, Map<String, String> columnMapping) {
        classColumnMapping.put(clazz, columnMapping);
    }

    public Map<Class, Map<String, String>> getClassColumnMapping() {
        return classColumnMapping;
    }

}
