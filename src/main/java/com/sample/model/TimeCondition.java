package com.sample.model;

public enum TimeCondition implements IEnum<String> {

    LAST("last"),
	MORE("more"),
	BETWEEN("between");

    private final String value;

    TimeCondition(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static TimeCondition getEnum(String value) {
        if(value == null)
            throw new IllegalArgumentException();
        for (TimeCondition v : values())
            if(value.equalsIgnoreCase(v.getValue())) return v;
        throw new IllegalArgumentException();
    }
}
