package com.sample.model.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.sample.model.TimeRange;
import com.sample.model.RangeCondition;

public abstract class AbstractGenericDAO {

    protected static final int DEFAULT_FETCH_LIMIT = 2000;

    protected static final String JPA_VARIABLE_PREFIX = ":";

    public static String getTimeRangeQuery(String field, TimeRange timeRange) {

        if (timeRange == null) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        if ( timeRange.getFrom() > 0 && timeRange.getTo() > 0 ) {
            builder.append( " AND " + field + " BETWEEN " + timeRange.getFrom() );
            builder.append( " AND " + timeRange.getTo() );
        }
        else if ( timeRange.getFrom() > 0 ) {
            builder.append( " AND " + field + " >= " + timeRange.getFrom() );
        }
        else if ( timeRange.getTo() > 0 ) {
            builder.append( " AND " + field + " <= " + timeRange.getTo());
        }

        return builder.toString();
    }

    protected static String getConditionFilter(String field, RangeCondition op,
            Object... value) {
        return getGenericConditionFilter(field, op, "AND", value);
    }

    protected static String getORConditionFilter(String field, RangeCondition op,
            Object... value) {
        return getGenericConditionFilter(field, op, "OR", value);
    }


    /**
     * @param field
     * @param op
     * @param value
     * @return
     */
    protected static String getGenericConditionFilter(String field, RangeCondition op, String queryOperator,
            Object... value) {
        if (op == null || value[0] == null
                || ( value[0] instanceof String && StringUtils.isEmpty(String.valueOf(value[0])))
                || (value[0] instanceof Collection && ((Collection)value[0]).isEmpty()) ) {
            return "";
        }

        if ( value[0] instanceof TimeRange) {
            return getTimeRangeQuery(field, (TimeRange) value[0]);
        }

        value[0] = replaceSpecialChars(value[0]);

        switch (op) {

        case EQ:
        case EQUAL:
            if ( value[0] instanceof Number || value[0] instanceof Boolean) {
                return " " + queryOperator + " " + field + " = " + value[0] + "";
            }
            return " " + queryOperator + " " + field + " = '" + String.valueOf(value[0]) + "'";

        case GE:
            if ( value[0] instanceof Double) {
                return " " + queryOperator + " " + field + " >= " + ((Double)value[0]).intValue() + "";
            }
            return " " + queryOperator + " " + field + " >= '" + String.valueOf(value[0]) + "'";

        case GT:
            if ( value[0] instanceof Double) {
                return " " + queryOperator + " " + field + " > " + ((Double)value[0]).intValue() + "";
            }
            return " " + queryOperator + " " + field + " > '" + String.valueOf(value[0]) + "'";

        case IN:
            return " " + queryOperator + " " + field + " IN " + getInValues(value);

        case LE:
            if ( value[0] instanceof Double) {
                return " " + queryOperator + " " + field + " <= " + ((Double)value[0]).intValue() + "";
            }
            return " " + queryOperator + " " + field + " <= '" + String.valueOf(value[0]) + "'";

        case LT:
            if ( value[0] instanceof Double) {
                return " " + queryOperator + " " + field + " < " + ((Double)value[0]).intValue() + "";
            }
            return " " + queryOperator + " " + field + " < '" + String.valueOf(value[0]) + "'";

        case NC:
            return " " + queryOperator + " " + field + " NOT IN " + getInValues(value);

        case NE:
            return " " + queryOperator + " (" + field + " != '" + String.valueOf(value[0]) + "' OR " + field + " IS NULL )";

        case BW:
            if ( value[0] instanceof Double &&  value[1] instanceof Double ) {
                return " AND (" + field + " BETWEEN " + ((Double)value[0]).intValue()
                        + " AND " + ((Double)value[1]).intValue() + ")";
            }
            return " AND (" + field + " BETWEEN '" + String.valueOf(value[0])
                    + "' AND '" + String.valueOf(value[1]) + "')";

        case LIKE:
            return " " + queryOperator + " " + field + " ILIKE '" + String.valueOf(value[0]).trim() + "'";

        case NLIKE:
            return " " + queryOperator + " " + field + " NOT LIKE '" + String.valueOf(value[0]).trim()
                    + "'";

        case LIKEWC:
            if (String.valueOf(value[0]).indexOf("*") > -1) {
                value[0] = String.valueOf(value[0]).replaceAll("\\*", "%");
            }
            return " " + queryOperator + " " + field + " ILIKE '" + String.valueOf(value[0]).trim() + "'";

        case NLIKEWC:
            if (String.valueOf(value[0]).indexOf("*") > -1) {
                value[0] =  String.valueOf(value[0]).replaceAll("\\*", "%");
            }
            return " " + queryOperator + " " + field + " NOT LIKE '" + String.valueOf(value[0]).trim()
                    + "'";
        }

        return "";
    }

    static String[] specialChars = {"'"};

    private static Object replaceSpecialChars(Object obj) {
        if (obj != null && obj instanceof String) {
            for (String specialChar : specialChars) {
                obj = ((String)obj).replaceAll(specialChar, "\\\\"+specialChar);
            }
        }
        return obj;
    }

    private static String getInValues(Object... value) {
        StringBuilder inClause = new StringBuilder();
        if (value[0] instanceof List) {
            List<String> list = (List<String>) value[0];
            inClause.append(" (");
            for (int i = 0; i < list.size(); i++) {
                if (i > 0) {
                    inClause.append( "," );
                }
                inClause.append( "'" + String.valueOf(list.get(i)) + "'" );
            }
            inClause.append(")");
            return inClause.toString();

        }
        //support multiple values
        else {
            inClause.append(" (");
            for (int i = 0; i < value.length; i++) {
                if (i > 0) {
                    inClause.append( "," );
                }
                inClause.append( "'" + String.valueOf(value[i]) + "'" );

            }
            inClause.append(")");

        }
        return inClause.toString();

    }

    protected int getResultLimit(int... limit) {
        int maxResults = DEFAULT_FETCH_LIMIT;

        if (limit != null && limit.length > 0 && limit[0] > 0) {
            maxResults = limit[0];
        }
        return maxResults;
    }

    /**
     * Replaces templates with the value from matching keys in the replacement map
     * If no replacement key is found on the map, empty string is replaced.
     * template text should be marked with ${}. e.g. ${subscriber}
     * @param templateText
     * @param replacements
     * @return text replaced with strings
     */
    public String replaceTemplateText(String templateText, Map<String,String> replacements) {

        if (replacements == null) {
            return templateText;
        }

        templateText = replacePattern("\\$\\{(.+?)\\}", templateText, replacements);
        return replaceJPAVariables(templateText, replacements);
    }

    private String replacePattern(String patternStr, String templateText, Map<String,String> replacements) {
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(templateText);

        StringBuilder builder = new StringBuilder();
        int i = 0;
        while (matcher.find()) {
            String replacement = replacements.get(matcher.group(1));
            builder.append(templateText.substring(i, matcher.start()));
            if (replacement != null) {
                builder.append(replacement);
            }

            i = matcher.end();
        }
        builder.append(templateText.substring(i, templateText.length()));
        return builder.toString();
    }

    private String replaceJPAVariables(String templateText, Map<String,String> replacements) {
        if (templateText != null && replacements != null) {
            for (Entry<String, String> entry : replacements.entrySet()) {
                templateText = templateText.replaceAll(JPA_VARIABLE_PREFIX + entry.getKey(), entry.getValue());
            }
        }
        return templateText;
    }

    protected String getHavingQuery(String field, HavingFunction function, Number value, RangeCondition op) {
        if ( field == null || function == null || value == null || op == null ) {
            return "";
        }

        return (" " + function + "(" + field + ")" + getHavingCondition(value, op));
    }


    protected String getHavingCondition(Number value, RangeCondition op) {
        if ( value == null || op == null ) {
            return "";
        }
        if ( op == RangeCondition.EQ ||  op == RangeCondition.EQUAL) {
            return " = " + value;
        }
        if ( op == RangeCondition.NE ) {
            return " != " + value;
        }
        if ( op == RangeCondition.LT ) {
            return " < " + value;
        }
        if ( op == RangeCondition.GT ) {
            return " > " + value;
        }
        return "";
    }

}
