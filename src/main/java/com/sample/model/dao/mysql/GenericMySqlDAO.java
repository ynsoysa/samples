package com.sample.model.dao.mysql;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.ResultSetHandler;
import org.sql2o.Sql2o;

import com.sample.NextUserApplication;
import com.sample.exceptions.PersistenceRuntimeException;
import com.sample.model.constants.RangeCondition;
import com.sample.model.dao.AbstractGenericDAO;
import com.sample.model.dao.HavingFunction;
import com.sample.model.dao.factories.MySqlToOFactory;
import com.sample.model.dao.interfaces.IBaseDAO;
import com.sample.model.dao.procedures.DatabaseObject;
import com.sample.model.dao.procedures.Procedure;
import com.sample.model.dao.procedures.ProcedureProvider;
import com.sample.model.dao.procedures.parameters.ProcedureParameter;
import com.sample.model.mysql.CustomResultSetHandler;
import com.sample.model.mysql.QueryFilterBuilder;
import com.sample.model.mysql.annotation.ExcludeSelect;
import com.sample.model.mysql.annotation.MultiClassColumnSetMapper;
import com.sample.model.mysql.annotation.MultiObjectResultWrapper;
import com.sample.resources.BasicResource;

public abstract class GenericMySqlDAO<Type extends BasicResource, KeyType> extends AbstractGenericDAO implements IBaseDAO<Type> {

    public static final String PROCEDURE_LOCATION = "sys";
    protected Class<Type> clazz;

    protected Class<KeyType> keyType;

    protected String wid;

    protected boolean activeUsers;

    protected Boolean anonymous;

    private static final Logger LOGGER = LoggerFactory.getLogger(GenericMySqlDAO.class);

    protected static String FIND_BY_ID_QUERY = "SELECT ${SELECT_COLUMNS} FROM ${TABLE_NAME} WHERE ${KEY_COLUMN} = ${KEY_VALUE}";
    protected static String FIND_ALL = "SELECT ${SELECT_COLUMNS} FROM ${TABLE_NAME} limit ${RESULT_LIMIT}";
    protected static String INSERT = "INSERT INTO ${TABLE_NAME} (${COLUMN_LIST}) VALUES (${VALUE_LIST})";
    protected static String UPDATE = "UPDATE ${TABLE_NAME} SET ${UPDATE_SETS} WHERE ${KEY_COLUMN} = ${KEY_VALUE}";
    protected static String FILTER_KEYS = "SELECT ${DISTINCT} ${KEY_COLUMN} FROM ${TABLE_NAME} WHERE 1=1 ${FILTER_EXPRESSION}";
    //This is not generic at the moment.
    protected static String FILTER_NEGATIVE_KEYS = "SELECT s.id subscriber_id from subscribers s LEFT OUTER JOIN "
            + "(SELECT ${DISTINCT} ${KEY_COLUMN} FROM ${TABLE_NAME} WHERE 1=1 ${FILTER_EXPRESSION} "
            + ") positive on s.id = positive.subscriber_id and positive.subscriber_id IS NULL;";

    protected static String FILTER = "SELECT ${DISTINCT} ${SELECT_COLUMNS} FROM ${TABLE_NAME} WHERE 1=1 ${FILTER_EXPRESSION} LIMIT ${RESULT_LIMIT}";

    public GenericMySqlDAO(Class<Type> clazz, Class<KeyType> keyType,  String wid) {
        this.clazz = clazz;
        this.keyType = keyType;
        this.wid = wid;
    }


    @Override
    public boolean isActiveUsers() {
        return activeUsers;
    }

    @Override
    public void setActiveUsers(boolean activeUsers) {
        this.activeUsers = activeUsers;

    }

    public String getWid() {
        return wid;
    }

    public void setWid(String wid) {
        this.wid = wid;
    }

    public Boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(Boolean anonymous) {
        this.anonymous = anonymous;
    }


    @Override
    public Type findById(Object id) {

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("TABLE_NAME", getTableName());
        params.put("KEY_COLUMN", getKeyColumn());
        params.put("KEY_VALUE", getSQLValue(id));
        params.put("SELECT_COLUMNS", getSelectColumns());

        return fetchFirst(FIND_BY_ID_QUERY, params, clazz);
    }

    @Override
    public List<Type> findAll(int offset, int... limit) {

        int maxResults = getResultLimit(limit);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("TABLE_NAME", getTableName());
        params.put("RESULT_LIMIT", String.valueOf(maxResults));
        params.put("SELECT_COLUMNS", getSelectColumns());

        return fetchAll(FIND_ALL, params, clazz);
    }

    //Query string

    public <T1> T1 executeSingleResultQuery(String query, Map<String, Object> params, Class<T1> returnType) {
        return fetchFirst(
                query,
                convertToSQLParamMap(params),
                returnType);
    }

    public <T1> List<T1> executeQuery(String query, Map<String, Object> params, Class<T1> returnType) {
        return fetchAll(
                query,
                convertToSQLParamMap(params),
                returnType);
    }

    public List<MultiObjectResultWrapper> executeQuery(String query, Map<String, Object> params, String resultSetMappingName) {
        return fetchAll(
                query,
                params,
                resultSetMappingName);
    }

    // Named Native Queries

    public <T1> T1 executeSingleResultNamedNativeQuery(String queryName, Map<String, Object> params, Class<T1> returnType) {
        return fetchFirst(
                getNamedNativeQuery(queryName),
                convertToSQLParamMap(params),
                returnType);
    }

    public <T1> List<T1> executeNamedNativeQuery(String queryName, Map<String, Object> params, Class<T1> returnType) {
        return fetchAll(
                getNamedNativeQuery(queryName),
                convertToSQLParamMap(params),
                returnType);
    }

    public List<MultiObjectResultWrapper> executeNamedNativeQuery(String queryName, Map<String, Object> params, String resultSetMappingName) {
        return fetchAll(
                getNamedNativeQuery(queryName),
                params,
                resultSetMappingName);
    }


    //Queries from text files

    public <T1> T1 executeSingleResulQueryFromFile(String filePath, Map<String, Object> params, Class<T1> returnType) {
        try {
            return fetchFirst(
                    getSQLFromFile(filePath),
                    convertToSQLParamMap(params),
                    returnType);
        } catch (IOException e) {
            LOGGER.error("Failed to run the query from file. File path : " + filePath, e);
           throw new PersistenceRuntimeException("Failed to run the query from file. File path : "
                   + filePath + " Cause: " + e.getMessage());
        }
    }

    public <T1> List<T1> executeQueryFromFile(String filePath, Map<String, Object> params, Class<T1> returnType) {
        try {
            return fetchAll(
                    getSQLFromFile(filePath),
                    convertToSQLParamMap(params),
                    returnType);
        } catch (IOException e) {
            LOGGER.error("Failed to run the query from file. File path : " + filePath, e);
           throw new PersistenceRuntimeException("Failed to run the query from file. File path : "
                   + filePath + " Cause: " + e.getMessage());
        }
    }

    public List<MultiObjectResultWrapper> executeQueryFromFile(String filePath, Map<String, Object> params, String resultSetMappingName) {
        try {
            return fetchAll(
                    getSQLFromFile(filePath),
                    params,
                    resultSetMappingName);
        } catch (IOException e) {
            LOGGER.error("Failed to run the query from file. File path : " + filePath, e);
           throw new PersistenceRuntimeException("Failed to run the query from file. File path : "
                   + filePath + " Cause: " + e.getMessage());
        }
    }



    //Filtering

    public List<Type> filter(QueryFilterBuilder builder) {

        if (builder.isNegative()) {
            LOGGER.error("This method does not support negative query");
            throw new PersistenceRuntimeException("This method does not support negative query");
        }

        int maxResults = getResultLimit(builder.getResultLimit());

        HashMap<String, String> replacements = new HashMap<String, String>();
        replacements.put(
                "KEY_COLUMN",
                getKeyColumn());
        replacements.put(
                "SELECT_COLUMNS",
                getSelectColumns());
        replacements.put(
                "TABLE_NAME",
                getTableName());
        replacements.put(
                "DISTINCT",
                builder.isDistinct() == true ? "DISTINCT" : "");

        replacements.put(
                "FILTER_EXPRESSION",
                getFilterExpression(builder)
                );

        replacements.put(
                "RESULT_LIMIT",
                String.valueOf(maxResults));

        return fetchAll(FILTER, replacements, clazz);
    }

    public List<KeyType> filterKeys(QueryFilterBuilder builder) {
        return filterAnyColumn(builder, keyType);
    }

    public <T1> List<T1> filterAnyColumn(QueryFilterBuilder builder, Class<T1> columnType) {

        int maxResults = getResultLimit(builder.getResultLimit());

        HashMap<String, String> replacements = new HashMap<String, String>();

        String keyColumn = builder.getNativeKeyColumn();
        if (keyColumn == null) {
            keyColumn = builder.getKeyColumn() != null ? getColumnNameForField(builder.getKeyColumn()) : getKeyColumn();
        }

        replacements.put(
                "KEY_COLUMN",
                keyColumn
                );
        replacements.put(
                "TABLE_NAME",
                getTableName());
        replacements.put(
                "DISTINCT",
                builder.isDistinct() == true ? "DISTINCT" : "");
        replacements.put(
                "FILTER_EXPRESSION",
                getFilterExpression(builder)
                );

        replacements.put(
                "RESULT_LIMIT",
                String.valueOf(maxResults));

        return fetchAll(
                builder.isNegative() ? FILTER_NEGATIVE_KEYS : FILTER_KEYS,
                replacements,
                columnType);
    }


    private String getFilterExpression(QueryFilterBuilder builder) {

        String filterEx = "";

        if (!builder.getFilters().isEmpty()) {
            filterEx +=  builder
            .getFilters()
            .stream()
            .map(
                    qf -> getJPAConditionFilter(
                            qf.isNativeFilter(),
                            qf.getColumn(),
                            qf.getOperator(),
                            qf.getFromValue(),
                            qf.getToValue()))
            .collect(Collectors.joining(" "));
        }

        if (!builder.getAggregateFilters().isEmpty()) {
            filterEx += " GROUP BY " + builder.getAggregateFilters().stream()
                    .map(
                            af ->  getColumnNameForField(af.getColumn()))
                    .collect(Collectors.joining(" , "));

            filterEx += " HAVING " + builder.getAggregateFilters().stream()
            .map(
                    af ->  getJPAAggregationFilter(
                            af.getColumn(),
                            af.getAggregateFunction(),
                            af.getOperator(),
                            af.getFromValue()))
            .collect(Collectors.joining(" "));
        }

        return filterEx;
    }

    private String getJPAConditionFilter(boolean nativeFilter, String column,
            RangeCondition operator, Object fromValue, Object toValue) {
        if (!nativeFilter) {
            column = getColumnNameForField(column);
        }

        return getConditionFilter(column, operator, fromValue, toValue);
    }

    private String getJPAAggregationFilter(String column,
            HavingFunction function, RangeCondition operator, Object value) {

        return getHavingQuery(
                getColumnNameForField(column),
                function, (Number)value, operator);
    }


    // Upserts

    public void create(Type object) {

        String queryString = generateInsertSQL(object);
        if (isShowMysqlQueries()) {
            LOGGER.info("MySQL Query : " + queryString);
        }
        Sql2o dataAccess = MySqlToOFactory.getInstance().getDataAccess(wid);
        try(Connection conn = dataAccess.open()){
            org.sql2o.Query query = conn.createQuery(queryString);
            query.executeUpdate();
            conn.commit();
        }
    }

    public void update(Type object) {

        String queryString = generateUpdateSQL(object);
        if (isShowMysqlQueries()) {
            LOGGER.info("MySQL Query : " + queryString);
        }

        Sql2o dataAccess = MySqlToOFactory.getInstance().getDataAccess(wid);
        try(Connection conn = dataAccess.open()){
            org.sql2o.Query query = conn.createQuery(queryString);
            query.executeUpdate();
            conn.commit();
        }
    }

    public void executeUpdateQuery(String queryString) {
        Sql2o dataAccess = MySqlToOFactory.getInstance().getDataAccess(wid);
        try(Connection conn = dataAccess.open()){
            org.sql2o.Query query = conn.createQuery(queryString);
            query.executeUpdate();
            conn.commit();
        }
    }

    //Fetch methods - private methods

    private <T1> T1 fetchFirst(String queryString, Map<String, String> params, Class<T1> returnType) {
        Sql2o dataAccess = MySqlToOFactory.getInstance().getDataAccess(wid);
        try(Connection conn = dataAccess.open()){
            String finalQuery = replaceTemplateText(queryString, params);
            if (isShowMysqlQueries()) {
                LOGGER.info("MySQL Query : " + finalQuery);
            }
            org.sql2o.Query query = conn
                    .createQuery(
                            finalQuery);
            query.setColumnMappings(getDefaultColumnMapping());
            return query.executeAndFetchFirst(returnType);
        }
    }

    private <T1> List<T1> fetchAll(String queryString, Map<String, String> params, Class<T1> returnListType) {
        Sql2o dataAccess = MySqlToOFactory.getInstance().getDataAccess(wid);
        try(Connection conn = dataAccess.open()){
            String finalQuery = replaceTemplateText(queryString, params);
            if (isShowMysqlQueries()) {
                LOGGER.info("MySQL Query : " + finalQuery);
            }
            org.sql2o.Query query = conn
                    .createQuery(
                            finalQuery);
            query.setColumnMappings(getDefaultColumnMapping());
            return query.executeAndFetch(returnListType);
        }
    }

    /**
     *
     * @param queryString
     * @param params
     * @param resultSetMappingName - Name of @SqlResultSetMapping
     * @return
     */
    protected List<MultiObjectResultWrapper> fetchAll(
            String queryString,
            Map<String, Object> params,
            String resultSetMappingName) {

        Sql2o dataAccess = MySqlToOFactory.getInstance().getDataAccess(wid);
        try(Connection conn = dataAccess.open()){
            String finalQuery = replaceTemplateText(
                    queryString,
                    convertToSQLParamMap(params));

            if (isShowMysqlQueries()) {
                LOGGER.info("MySQL Query : " + finalQuery);
            }
            org.sql2o.Query query = conn
                    .createQuery(
                            finalQuery);

            ResultSetHandler<MultiObjectResultWrapper> resultSetHandler =
                    new CustomResultSetHandler(getColumnMapping(resultSetMappingName));

            return query.executeAndFetch(resultSetHandler);
        }
    }

     //Utility and helper methods

    private String getSQLValue(Object object) {

        if (object instanceof Number) {
            return String.valueOf(object);
        }

        if (object instanceof Boolean) {
            return String.valueOf(object);
        }

        if (object instanceof Collection
                || object instanceof List) {
            return (String) ((Collection)object)
                    .stream().map(e -> "'"+ String.valueOf(e) + "'")
                    .collect(Collectors.joining(","));
        }

        return "'" + String.valueOf(object) + "'";
    }

    //**********************************************************
    //  Method uses reflection
    //**********************************************************

    public String getTableName() {
        try {
            Table table = clazz.getAnnotation(Table.class);
            if (table != null){
                return table.name();
            }
        } catch (Exception e) {
        }
        return null;
    }

    public String getKeyColumn(){
        for(Field field : clazz.getDeclaredFields()){
            //get name by ID field
            Id id = field.getAnnotation(Id.class);
            if (id != null) {
                Column column = field.getAnnotation(Column.class);
                if (column != null) {
                    return column.name();
                }
                return field.getName();
            }
        }
        LOGGER.error("@Id is not defined for the class : " + clazz);
        throw new PersistenceRuntimeException("@Id is not defined for the class : " + clazz);
    }

    public String getNamedNativeQuery(String queryName) {
        NamedNativeQueries queries = clazz.getAnnotation(NamedNativeQueries.class);
        if (queries != null) {
            NamedNativeQuery[] queryArray = queries.value();
           try {
            return Arrays
                       .stream(queryArray)
                       .filter(q -> q.name().equals(queryName))
                       .map(q -> q.query())
                       .findFirst()
                       .get();
            } catch (NoSuchElementException e) {
                LOGGER.error("Invalid query name. Check @NamedNativeQuery with name "
                        + queryName + " is defined for the class : " + clazz);
                throw new PersistenceRuntimeException("Invalid query name. Check @NamedNativeQuery with name "
                        + queryName + " is defined for the class : " + clazz);
            }
        }
        LOGGER.error("No native named queries defined for the class : " + clazz);
        throw new PersistenceRuntimeException("No native named queries defined for the class : " + clazz);

    }


    public String getSelectColumns() {

        List<String> includeCols = new ArrayList<>();
        for(Field field : clazz.getDeclaredFields()){
            Column column = field.getAnnotation(Column.class);
            ExcludeSelect excludeCol = field.getAnnotation(ExcludeSelect.class);
            if (column != null && excludeCol == null) {
                includeCols.add(column.name());
            }
        }

        if (!includeCols.isEmpty()) {
            return includeCols
                    .stream()
                    .collect(Collectors.joining(","));
        }

        return "*";
    }

    /**
     * This is the mapping between sql column name and the object attribute.
     * This is used by SQLO2 for populating the object.
     * @return
     */
    public Map<String, String> getDefaultColumnMapping() {
        Map<String, String> mapping = new HashMap<>();
        for(Field field : clazz.getDeclaredFields()){
            String fieldName = field.getName();
            Column column =field.getAnnotation(Column.class);
            ExcludeSelect excludeSelect = field.getAnnotation(ExcludeSelect.class);
            if (excludeSelect == null
                    && column != null
                    && column.name() != null) {
                mapping.put(column.name(), fieldName);
            }
        }
        return mapping;
    }

    public MultiClassColumnSetMapper getColumnMapping(String mappingName) {
        MultiClassColumnSetMapper classColumnSetMapper = new MultiClassColumnSetMapper();

        SqlResultSetMappings resultSetMappings = clazz.getAnnotation(SqlResultSetMappings.class);
        if (resultSetMappings == null) {
            LOGGER.error("@SqlResultSetMappings annotation is missing in the class declaration.");
            throw new PersistenceRuntimeException("@SqlResultSetMappings annotation is missing in the class declaration.");
        }

        SqlResultSetMapping resultSetMapping =
                Arrays
                .stream(resultSetMappings.value())
                .filter(m -> m.name().equals(mappingName))
                .findFirst()
                .get();

        //support only a single @SqlResultSetMapping for the moment
        if (resultSetMapping == null) {
            LOGGER.error("Invalid mapping name. Name : " + mappingName);
            throw new PersistenceRuntimeException("Invalid mapping name. Name : " + mappingName);
        }

        EntityResult[] entityResults = resultSetMapping.entities();
        for (EntityResult entityResult : entityResults) {
            Class entityClass = entityResult.entityClass();
            FieldResult[] fieldResults = entityResult.fields();
            if (fieldResults == null || fieldResults.length == 0) {
                classColumnSetMapper.addClassColumnMapper(entityClass, getDefaultColumnMapping());
            }
            else {
                Map<String, String> colMapping = new HashMap<>();
                for (FieldResult fieldResult : fieldResults) {
                    colMapping.put(fieldResult.column(), fieldResult.name());
                }
                classColumnSetMapper.addClassColumnMapper(entityClass, colMapping);
            }
        }

        return classColumnSetMapper;
    }

    public String getColumnNameForField(String filedName) {
        String columnName = null;
        for(Field field : clazz.getDeclaredFields()){
            if (field.getName().equals(filedName)) {
                columnName = field.getName();
                Column column = field.getAnnotation(Column.class);
                if (column != null) {
                    columnName = column.name();
                }
                JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);
                if (joinColumn != null) {
                    columnName = joinColumn.name();
                }
                break;
            }
        }

        if (columnName == null) {
            LOGGER.error("Unable to find property " + filedName + " in class : " + clazz);
            throw new PersistenceRuntimeException("Unable to find property " + filedName + " in class : " + clazz);
        }

        return columnName;

    }

    public Map<String, Object> getColumnValueMapping(Type object) {
        Map<String, Object> mapping = new HashMap<String, Object>();

        try {
            for(Field field : object.getClass().getDeclaredFields()){
                String fieldName = field.getName();

                Column column = field.getAnnotation(Column.class);
                if (column != null) {
                    String columnName = column.name();
                    Object fieldValue = PropertyUtils.getProperty(object, fieldName);
                    mapping.put(columnName, fieldValue);

                    LOGGER.debug("Field: " + fieldName
                            + " Coulumn Name:" + columnName
                            + "value: " + fieldValue);
                }
            }

            return mapping;
        } catch (Exception e) {
            LOGGER.error("Error occured while creating column value mapping for class : " + clazz, e);
            throw new PersistenceRuntimeException("Error occured while creating column value mapping for class : " + clazz);
        }
    }

    public Object getKeyValue(Type object) throws PersistenceRuntimeException {

        try {
            for(Field field : object.getClass().getDeclaredFields()){
                String fieldName = field.getName();

                Annotation[] annotations = field.getDeclaredAnnotations();

                for (Annotation annotation : annotations) {
                    if (annotation.annotationType().equals(Id.class)) {
                        return PropertyUtils.getProperty(object, fieldName);
                    }
                }
            }

        } catch (Exception e) {
            LOGGER.error("Error occured while creating column value mapping for class : " + clazz, e);
            throw new PersistenceRuntimeException("Error occured while creating column value mapping for class : " + clazz);
        }
        LOGGER.error("@Id is not defined for the class : " + clazz);
        throw new PersistenceRuntimeException("@Id is not defined for the class : " + clazz);
    }

    protected String generateSelectQuery(Object id) {
        HashMap<String, String> replacements = new HashMap<String, String>();
        replacements.put("TABLE_NAME", getTableName());
        replacements.put("KEY_COLUMN", getKeyColumn());
        replacements.put("KEY_VALUE", getSQLValue(id));
        replacements.put("SELECT_COLUMNS", getSelectColumns());

        String queryString = replaceTemplateText(FIND_BY_ID_QUERY, replacements);
        return queryString;
    }

    protected String getSQLFromFile(String filePath) throws IOException {
        return new String(Files.readAllBytes(Paths.get(filePath)));
    }


    protected Map<String, String> convertToSQLParamMap(Map<String, Object> params) {
        if (params == null) {
            return new HashMap<>();
        }
        Map<String, String> sqlMap = params.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                                          e -> getSQLValue(e.getValue())));
        return sqlMap;
    }

    protected String generateInsertSQL(Type object) {
        Map<String, Object> mapping = getColumnValueMapping(object);

        HashMap<String, String> replacements = new HashMap<String, String>();
        replacements.put("TABLE_NAME", getTableName());
        replacements.put("COLUMN_LIST",
                mapping
                .keySet()
                .stream()
                .collect(
                        Collectors
                        .joining(",")));
        replacements.put("VALUE_LIST",
                mapping
                .values()
                .stream()
                .map(o -> getSQLValue(o))
                .collect(Collectors.joining(",")));


        return replaceTemplateText(INSERT, replacements);
    }

    protected String generateUpdateSQL(Type object) {
        Map<String, Object> mapping = getColumnValueMapping(object);

        HashMap<String, String> replacements = new HashMap<String, String>();
        String keyColumn = getKeyColumn();
        replacements.put("KEY_COLUMN", keyColumn);
        replacements.put("KEY_VALUE",  getSQLValue(getKeyValue(object)));
        replacements.put("TABLE_NAME", getTableName());
        replacements.put("UPDATE_SETS", mapping
                .entrySet()
                .stream()
                .filter(en -> !en.getKey().equals(keyColumn))
                .map(en -> en.getKey() + " = " + getSQLValue(en.getValue()))
                .collect(Collectors.joining(","))
                );

        return replaceTemplateText(UPDATE, replacements);
    }

    protected boolean isShowMysqlQueries() {
        return NextUserApplication.isShowMySQLQuery();
    }

    public String escape(String stringArgument) {
        if(stringArgument == null || "".equals(stringArgument)){
            return "null";
        }
        return "'" + stringArgument + "'";
    }

    protected static String getConditionFilter(String field, RangeCondition op,
            Object... value) {
        return getGenericConditionFilter(field, op, "AND", value);
    }

    protected static String getGenericConditionFilter(String field, RangeCondition op, String queryOperator,
            Object... value) {
        if (op == null || value[0] == null
                || ( value[0] instanceof String && StringUtils.isEmpty(String.valueOf(value[0])))
                || (value[0] instanceof Collection && ((Collection)value[0]).isEmpty()) ) {
            return "";
        }

        switch (op) {
            case LIKE:
                return " " + queryOperator + " LOWER(" + field + ") LIKE LOWER('" + String.valueOf(value[0]).trim() + "')";

            case LIKEWC:
                if (String.valueOf(value[0]).indexOf("*") > -1) {
                    value[0] = String.valueOf(value[0]).replaceAll("\\*", "%");
                }
                return " " + queryOperator + " LOWER(" + field + ") LIKE LOWER('" + String.valueOf(value[0]).trim() + "')";

        }

        return AbstractGenericDAO.getGenericConditionFilter(field, op, queryOperator, value);
    }

    protected List<Long> executeProcedure(Procedure procedure, ProcedureParameter params){
        DatabaseObject procedurePointer = new DatabaseObject(PROCEDURE_LOCATION, procedure);
        ProcedureProvider<Long> provider = new ProcedureProvider<>(procedurePointer, params.getOrderedParameters(),
                MySqlToOFactory.getInstance(), Long.class);
        return provider.executeProcedure();
    }
}
