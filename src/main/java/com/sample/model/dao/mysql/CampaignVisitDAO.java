package com.sample.model.dao.mysql;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.sample.model.mysql.QueryFilterBuilder;
import com.sample.model.mysql.transition.PageviewHistory;
import com.sample.model.search.CampaignVisitSearchDO;
import com.sample.model.search.CampaignVisitSearchDO.Keyword;

public class CampaignVisitDAO  extends GenericMySqlDAO<PageviewHistory, Long> {

    private static final String KEYWORD_COLUMN = "url";

    public CampaignVisitDAO(String wid, Boolean anonymous) {
        super(PageviewHistory.class, Long.class, wid);
        this.anonymous = anonymous;
    }

    public List<Long> findSubscribersByCampVisits(CampaignVisitSearchDO searchDO, boolean negative) {
        QueryFilterBuilder queryFilterBuilder = new QueryFilterBuilder()
        .distinct(true)
        .negative(negative)
        .keyColumn("subscribers")
        .addTimeRangeFilter("ts", searchDO.getTimeRange());

      //keyword search
        //from keyword group
        for (Keyword keyword : searchDO.getFromGroups()) {
            queryFilterBuilder = addKeywordFilter(queryFilterBuilder, keyword);
        }
        //search keyword group
        queryFilterBuilder = addKeywordFilter(queryFilterBuilder, searchDO.getSearchGroup());

        //via keyword group
        queryFilterBuilder = addKeywordFilter(queryFilterBuilder, searchDO.getViaGroup());

        //search keyword group
        queryFilterBuilder = addKeywordFilter(queryFilterBuilder, searchDO.getClicksGroup());

        return filterAnyColumn(queryFilterBuilder, Long.class);
    }


    /**
     * Adds the keyword to the search query if keyword is not null
     * @param builder
     * @param keyword
     */
    private QueryFilterBuilder addKeywordFilter(QueryFilterBuilder queryFilterBuilder, Keyword keyword) {
        if (keyword != null
                && StringUtils.isNotBlank(keyword.getName())
                && StringUtils.isNotBlank(keyword.getValue()) ) {
            queryFilterBuilder.addNativeNewFilter(
                    KEYWORD_COLUMN,
                    "%" + keyword.getName() + "=" + keyword.getValue() + "%",
                    keyword.getCondition());
        }
        return queryFilterBuilder;
    }

}
