package com.sample.model;

import com.sample.model.BasicResource;
import org.codehaus.jackson.annotate.JsonProperty;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table( name = "pageview_history" )
public class PageviewHistory extends BasicResource {

    
    @Column(name="url")
    @JsonProperty("url")
    private String url ;

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name="ts")
    @JsonProperty("ts")
    private long ts ;

    @Column(name="visit_cookie")
    @JsonProperty("visit_cookie")
    private long visitCookie;

    @Column(name="time_on_page")
    @JsonProperty("time_on_page")
    private long timeOnPage;

    @Column(name="params")
    @JsonProperty("params")
    private String params;

    @Column(name="anonymous")
    @JsonProperty("anonymous")
    private boolean anonymous;

    @ManyToOne(optional = true,  fetch = FetchType.LAZY)
    @JoinColumn(name = "subscriber_id")
    private Subscribers subscribers;

    public Subscribers getSubscribers() {
        return this.subscribers;
    }

    public void setSubscribers(Subscribers subscribers) {
        this.subscribers = subscribers;
    }

    public void setUrl ( String url ) {
        this.url = url;
    }

    public String getUrl () {
        return this.url;
    }

    public void setTs ( long ts ) {
        this.ts = ts;
    }

    public long getTs () {
        return this.ts;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getParams() {
        return this.params;
    }

    public long getVisitCookie() {
        return visitCookie;
    }

    public void setVisitCookie(long visitCookie) {
        this.visitCookie = visitCookie;
    }

    public long getTimeOnPage() {
        return timeOnPage;
    }

    public void setTimeOnPage(long timeOnPage) {
        this.timeOnPage = timeOnPage;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }


}