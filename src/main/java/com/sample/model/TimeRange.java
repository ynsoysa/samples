package com.sample.model;

import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sample.exception.InvalidTimeRangeException;
import com.sample.model.TimeCondition;

public class TimeRange implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeRange.class);

	private static final long serialVersionUID = 7259137553050297860L;

	private long from=0;

    private long to=0;

    private long timeGap=0;

    private Long current;

    public TimeRange () {
        this(0,0);
    }

    public TimeRange ( long from, long to ) {
        this(from, to, 0, null);
    }

    public TimeRange ( long from, long to, long timeGap, Long current ) {
        this.from = from;
        this.to = to;
        this.timeGap = timeGap;
        this.current = current;
    }

    @Override
    public TimeRange clone () {
        return new TimeRange(from, to, timeGap, current);
    }

    public long getFromNoGap() {
        return from;
    }

    public long getFrom() {
        return from - timeGap;
    }

    public void setFrom(long from) {
        this.from = from;
    }

    public long getTo() {
        return to - timeGap;
    }

    public long getToNoGap() {
        return from;
    }

    public void setTo(long to) {
        this.to = to;
    }

    @Override
    public String toString () {
        return "[TimeRange:" + new Date(getFrom()).toString() + "("+getFrom()+"):"
                + new Date(getTo()).toString()+ "("  +getTo()+") Gap: ("+timeGap+")]";
    }

    public static TimeRange get ( ITimeRangeable rule, ChronoUnit chron ) {
        return get(rule, chron, null);
    }

    public static TimeRange get ( ITimeRangeable rule, ChronoUnit chron, Long scheduled ) {
        Instant fromTs = null;
        Instant toTs = null;

        Instant now = Instant.now();

        if (scheduled != null && scheduled.longValue() > 0) {
            now = Instant.ofEpochMilli(scheduled);
        }


        if ( rule.getTimeCondition() == TimeCondition.LAST ) {
            fromTs = now.minus(rule.getInTheLast(), chron)
                        .truncatedTo(ChronoUnit.HOURS);
            toTs = now.truncatedTo(ChronoUnit.HOURS);
        } else if ( rule.getTimeCondition() == TimeCondition.MORE ) {
            toTs = now.minus(rule.getMoreThan(), chron)
                    .truncatedTo(ChronoUnit.HOURS);

        } else if ( rule.getTimeCondition() == TimeCondition.BETWEEN ) {
            fromTs = now.minus(rule.getBetweenFrom(), chron)
                    .truncatedTo(ChronoUnit.HOURS);
            toTs =now.minus(rule.getBetweenTo(), chron)
                    .truncatedTo(ChronoUnit.HOURS);
        }

        return new TimeRange( fromTs == null ? 0 :fromTs.toEpochMilli(), toTs.toEpochMilli() ) ;
    }

    public static boolean validate ( ITimeRangeable rule ) {

        TimeCondition timeCondition = rule.getTimeCondition();

        if ( timeCondition == TimeCondition.LAST && rule.getInTheLast() == -1) {
            LOGGER.warn(rule.getClass().getSimpleName() + " "
                    + rule.getId() + " not evaluable: TimeCondition.LAST requires inTheLast property to be set");
            throw new InvalidTimeRangeException(rule.getClass().getSimpleName() + " "
                    + rule.getId() + " not evaluable: TimeCondition.LAST requires inTheLast property to be set");
        }
        if ( timeCondition == TimeCondition.MORE && rule.getMoreThan() == -1) {
            LOGGER.warn(rule.getClass().getSimpleName() + " " + rule.getId()
                    + " not evaluable: TimeCondition.MORE_THAN requires moreThan property to be set");
            throw new InvalidTimeRangeException(rule.getClass().getSimpleName() + " " + rule.getId()
                    + " not evaluable: TimeCondition.MORE_THAN requires moreThan property to be set");
        }
        if ( timeCondition == TimeCondition.BETWEEN && (rule.getBetweenFrom() == -1 || rule.getBetweenTo() == -1)) {
            LOGGER.warn(rule.getClass().getSimpleName() + " " + rule.getId()
                    + " not evaluable: TimeCondition.BETWEEN requires getBetweenFrom and getBetweenTo properties to be set");
            throw new InvalidTimeRangeException(rule.getClass().getSimpleName() + " " + rule.getId()
                    + " not evaluable: TimeCondition.BETWEEN requires getBetweenFrom and getBetweenTo properties to be set");
        }

        if ( timeCondition == TimeCondition.BETWEEN && rule.getBetweenFrom() < rule.getBetweenTo() ) {
            LOGGER.warn(rule.getClass().getSimpleName() + " " + rule.getId()
                    + " not evaluable: TimeCondition.BETWEEN requires getBetweenFrom to be greater than getBetweenTo (between )");
            throw new InvalidTimeRangeException(rule.getClass().getSimpleName() + " " + rule.getId()
                    + " not evaluable: TimeCondition.BETWEEN requires getBetweenFrom to be greater than getBetweenTo (between )");
        }

        return true;
    }

    public long getTimeGap() {
        return timeGap;
    }

    public void setTimeGap(long timeGap) {
        this.timeGap = timeGap;
    }

    public Long getCurrent() {
        return current;
    }

    public void setCurrent(Long current) {
        this.current = current;
    }

    public String getFromString(){
        return getProcedureRepresentation(from);
    }

    public String getToString(){
        return getProcedureRepresentation(to);
    }

    private String getProcedureRepresentation(long timestamp) {
        if (timestamp == 0){
            return "null";
        }
        return String.valueOf(timestamp);
    }

}
